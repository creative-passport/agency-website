---
title: "About"
date: 2018-07-30T16:14:07+01:00
draft: true
---

# About the Creative Passport

The Creative Passport is part of Mycelia, a project to reimagine how the music industry works, putting the creative, the artists back in the centre of things by giving them control over the data about themselves and their work.

By leveraging new ideas such as Distributed Identity, we are building the Creative Passport to enable you to create and control your online creative public persona. How the world sees you.

For more information on Mycelia and the Mycelia Tour visit the [Mycelia For Music](https://www.myceliaformusic.org "Mycelia For Music Homepage") Site.