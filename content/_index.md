---
title: "Supporting the Creative Passport"
date: 2018-07-30T16:04:39+01:00
background_color_class: "bg-black"
draft: true
---

# Supporting the Creative Passport

This site supports the Creative Passport network, a core part of the Mycelia vision for the music industry and other creative industries.

Here you will find support information, technical details and links to our developer resources and infrastructure dashboards.

Also, since Mycelia is focused on the music industry we will host Creative Passports for other industries here. 

These will include: 